========
Projects
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   project-management
   projects-portfolio
   project-joining
   project-proposing
   roles


What is an LSF project?
***********************

Libre Space Foundation is home to a range of open source space projects, each working with their own collaborative community style to create technologies compatible with the `Libre Space Manifesto <https://manifesto.libre.space>`_.

Within this documentation you can find details about the way Libre Space Foundation projects work, join the Foundation and are managed.



