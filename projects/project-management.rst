==================
Project Management
==================

.. contents::
   :local:
   :backlinks: none


Overview
--------

This is the recommended Mode of Operation for Libre Space Foundation projects.
GitLab issues shall be used as the platform for the project management of LSF projects.
Kanban board shall be utilized to facilitate the project management process.

Creation
--------

For any new or existing project generally there is an associated Gitlab project.
A new Gitlab project can be requested `here <https://gitlab.com/librespacefoundation/ops/support/-/issues/new>`_ using the "GitLab - Create project" template.


Kanban board
------------

The Kanban board shall have the following columns:

* Open
* To Do
* Doing
* Done (optional)
* Closed

Kanban process is task oriented.
So, all issues created shall involve a task or be expressed as tasks.


Open column
^^^^^^^^^^^

Once an issue is created, it goes in the Open column as a new card.
Cards which remain in this column shall always be unassigned.
The idea with this column is that people can have a quick visual indication on which issues are pending.
No GitLab label shall be used for this column


To Do column
^^^^^^^^^^^^

The backlog can grow quite large but the team has a maximum working capacity.
The 'To Do' column is used to limit the pending work which can be overwhelming for the team.
A set of cards is selected and moved to the 'To Do' column.
These are the issues that the team shall focus on.
Cards which remain in To Do state shall always be unassigned.
The idea with this column is that people can have a quick visual indication on which pending issues to focus on.
The set of issues in ready state shall be selected based on:

#. How ready are the issues to be worked on
#. If they all deliver a working feature
#. The future working capacity of the team.

The number of issues in this column shall be kept low and not more than the team can handle.
A GitLab label shall be created for this column.
The label shall have ``#F0AD4E`` as a background colour.


Doing column
^^^^^^^^^^^^

People can assign themselves issues using this column.
This shall be done by:

#. Pulling the card from 'To Do' to 'Doing' column and
#. Assigning the card to themselves.

Cards which remain in doing state shall always be assigned.
The idea with this column is that people can have a quick visual indication on which issues are worked on.
The number of issues in this column shall not exceed at any time the maximum working capacity of the team.
A GitLab label shall be created for this column.
The label shall have ``#5CB85C`` as a background colour.


Done column
^^^^^^^^^^^

Done column is optional.
If used, completed tasks shall be moved in this column.
Cards which remain in Done state shall always be assigned.
It is mostly useful in projects where a regular status meeting takes place.
During the meeting, completed tasks can be reviewed and closed.
A GitLab label shall be created for this column.
The label shall have ``#428BCA`` as a background colour.


Closed column
^^^^^^^^^^^^^

The Closed column is used for:

* Completed tasks, when Done column is not used.
* Canceled or superseded tasks.

Cards which remain in Done state shall be assigned when completed and unassigned when canceled.
A GitLab label shall be created for this column.
The label shall have ``#428BCA`` as a background colour.


Retrospective
-------------

The retrospective process is a method for teams to do self-inspection and continuously improve.
Retrospectives meetings shall be executed regularly on all projects.
The meeting shall result a concrete action plan towards improvement.


Rules
^^^^^

The retrospective meeting shall follow these rules:

#. A retrospective meeting shall be held once a month
#. A positive environment shall be created
#. The meeting shall focus on continuous improvement
#. Blaming people must be avoided
#. All opinions shall be listened


Process
^^^^^^^

The retrospective is a regular meeting.
The meeting shall be scheduled when most people can attend, taking into account the distribution of people across different timezones.
Although a physical meeting is ideal, an audio or realtime text meeting is also acceptable.
The retrospective process steps are the following:

0. Invite the team to join the meeting
1. Discuss about what went well since the last meeting
2. Discuss about what lessons were learned
3. Make a plan and create actions needed for going forward

The actions shall create or update tasks in GitLab.
