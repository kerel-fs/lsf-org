=================
Project Proposing
=================

.. contents::
   :local:
   :backlinks: none


Overview
--------

This is the recommended process to propose a new project regardless of scale within LSF. The overall proccess looks like this:
A Core contributor, contributor, or community participant has an idea, documents it, asks for feedback, submits for approval to LSF board, runs the project and then LSF core contributors and board review it on regular basis or at specific dates.

Procedure
---------

As a Core contributor you may have an idea about a specific project for LSF. It might be something new or building on top of existing projects and ideas.

.. note::
   This is a process to be followed by Core Contributors.
   If you are not a Core contributor but have an idea for a project, please reachout to one of the Core Contributors to help you.


1. Ideation
~~~~~~~~~~~~~~

The first step of this process, would be the immediate ideation among the person (or persons) that came up with the idea. It is important the following items to be checked:

* The proposed project pushes the Libre Space Manifesto forward and abides by it.
* The people involved could have the capacity (priorities considered) to undertake the project.
* An initial resources (budget and people allocation) is made for the project
* A set of goals is set that align with the overal LSF strategy
* A set of measurable metrics and impact is set to be evaluated in a 3-month period (or 6 for larger projects)
* Basic project roles are identified (see :doc:`roles`)


2. Documentation
~~~~~~~~~~~~~~~~

The person responsible for the project idea, documents it by filling a `Template Issue <https://gitlab.com/librespacefoundation/lsf-core/-/issues/new?issuable_template=project-proposing>`_ in our lsf-core private repository.


3. Feedback
~~~~~~~~~~~~~~~~

After the proposal issue is created, the proposing person, ensures that there is visibility for this proposal at a LSF Core level by:

* Posting on Core Contributors section in community.libre.space
* Posting on #lsf-core channel at Matrix
* Mentioning it on Monday and daily core group meetings
* Reaching out specifically to certain contributors to solicit feedback on the proposal

After discussions and possible feedback sessions are made then the points and possible alternation to the proposal should be reflected in the original proposal issue by the proposing person.

4. Approval
~~~~~~~~~~~~~~~~

When the proposing person believes that the feedback loop is complete, submits the project for approval to the lsf-board by adding a comment "Ready for review" at the proposing issue.

The lsf-board group meets weekly and will be reviewing any proposed proposals when they are ready.

Three different outcomes are possible: 1. Approval, in which case the project is commenced and issue is moved to Doing, 2. Rejection, where the issue is closed and tagged as Rejected, 3. More info needed, where a board member takes responsibility to have a feedback loop with the proposing person.


5. Evaluation
~~~~~~~~~~~~~~~~

After a fixed period of 3 (or 6) months, a full project impact and results evaluation should be conducted as a public presentation to lsf-core group as the responsibility of the assigned project champion. The project might be concluded, extended or made permanent based on lsf-board decision.
