=====
Board
=====

.. contents::
   :local:
   :backlinks: none

Libre Space Foundation Board is the governing body of LSF.
As the highest leadership body of the Foundation and to satisfy its fiduciary duties, the board is responsible for:

#. Determining the mission and purposes of the Foundation
#. Selecting and evaluating the performance of the chief executive
#. Strategic and organizational planning
#. Ensuring strong fiduciary oversight and financial management
#. Fundraising and resource development
#. Approving and monitoring the Foundation's programs and services
#. Enhancing the Foundation's public image
#. Assessing its performance as the governing body of the Foundation

Joining
-------

A new member joining the board requires an existing board member nomination and a unanimous decision among the current board members.
