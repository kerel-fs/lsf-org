======
People
======


Overview
--------
Libre Space Foundation is a vibrant organization comprised of multiple individuals around the world.
This section of the documentation caters to their needs, helping with definitions and processes.
Among others, it contains topics related to joining, training, engaging, coaching and cooperation between individuals.
Processes developed and documented in this section are meant to become useful tools for cultivating a common culture across the organization; a culture which is based on principles of self-management, self-governing, self-organizing and pure peer relationships.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   board
   core-contributors
   conflict-resolution
