:orphan:

===============================
Hardware Development Guidelines
===============================
.. contents::
   :local:
   :backlinks: none


Overview
--------
This guide refers to practices that is followed for hardware projects.
It contains 3 sections:

1. design
2. fabrication
3. assembly

Each of the sections refer to a different stage of hardware development.

Design
------

Repository
^^^^^^^^^^
The repository of a hardware project must include:

#. License file for example, `CERN Open Hardware Licence Version 2 - Strongly Reciprocal <https://gitlab.com/librespacefoundation/sidloc/sidloc-schematic/-/blob/master/LICENSE>`_
#. Libre Space Foundation `Contribution Guide <https://gitlab.com/librespacefoundation/sidloc/sidloc-schematic/-/blob/master/CONTRIBUTING.md>`_
#. Git ignore files for different kind of programms, like KiCad, FreeCad and QUCS.
#. In case of CAD files, e.g. FreeCad, it is recommended  to use `Git LFS <https://docs.gitlab.com/ee/topics/git/lfs/>`_
#. For project management follow, `recommended Mode of Operation for Libre Space Foundation projects <https://docs.libre.space/en/latest/projects/project-management.html>`_
#. An (under test) method to keep track of issues with epics and/or milestones is followed at `SIDLOC project - issue <https://gitlab.com/librespacefoundation/lsf-org/-/issues/32>`_ and some `notes <https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-org/-/issues/177>`_

KiCad and FreeCad Workflow
^^^^^^^^^^^^^^^^^^^^^^^^^^
#. KiCad workflow for multiple boards is under test in `SIDLOC project <https://gitlab.com/librespacefoundation/sidloc>`_ and here is the `description of method <https://gitlab.com/librespacefoundation/sidloc/sidloc-schematic/-/issues/2>`_
#. FreeCad workflow uses `Git LFS - File Locking <https://docs.gitlab.com/ee/topics/git/lfs/#file-locking>`_

Electronics Parts
^^^^^^^^^^^^^^^^^
1. An inventory with electronic parts is kept in LSF lab. For that reason it is recommended to select parts
   from inventory that the threshold of minimum order is above 20 (more details TBA)
2. Parts like MCUs, PSUs etc are preferred to re-used for all the projects
3. Preferred `vendors <https://docs.libre.space/en/latest/procurement.html>`_
4. For Board to wire connectors the Molex - PicoBlade and Hirose - DF11 series are preferred 
5. A `Libre Space Foundation KiCad Library <https://gitlab.com/librespacefoundation/lsf-kicad-lib>`_ exists. It is advised to use parts from it (or KiCAD libs). In case that
   a part does not exist a merge request in to library is more than welcome
6. To export a BOM, for e.g. Mouser, use either bom_csv_grouped_by_value and from generated .csv use the second table of follow a guide: `The Generation of Bill of Material Files <https://www.raypcb.com/the-method-to-generate-centroid-file-and-bom-from-kicad/>`_

KiCAD Preferred settings
^^^^^^^^^^^^^^^^^^^^^^^^
1. Schematic grid size: 1.27mm
2. PCB Logos: project logo, license logo and Libre Space Foundation logo.
3. CI for `KiCad (Under development) <https://gitlab.com/librespacefoundation/sidloc/sidloc-org/-/issues/123>`_
4. Add Fiducial points that help stencil alignment in stencil printer

FreeCad Preferred settings
^^^^^^^^^^^^^^^^^^^^^^^^^^
1. For fabrication drawings use `Libre Space Foundation templates <https://gitlab.com/librespacefoundation/lsf-templates/-/tree/master/freecad-template>`_

Releases
^^^^^^^^
The release scheme is under development and testing in `SatNOGS COMMS project <https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/merge_requests/176>`_

Fabrication
-----------

PCB Ordering
^^^^^^^^^^^^
1. Preferred `vendors <https://docs.libre.space/en/latest/procurement.html>`_ are to be followed
2. Tag a release on the hardware repository and write all production details like `this <https://gitlab.com/librespacefoundation/cronos-rocket/sky-runner-payload/sky-runner-hw/-/tags/v1.0.0>`_ .
3. Ping people with access according to Procurement policy.
4. Note: Stencil size (maximum) 180x220 mm (due to stencil printer)

Assembly
--------

PCB assembly
^^^^^^^^^^^^
Libre Space Foundation uses `hackerspace.gr laboratory <https://www.hackerspace.gr/>`_ with:

1. Reflow oven, TBD
2. Pick and Place, TBD
3. Stencil printer, TBD


Mechanichal Parts assembly
^^^^^^^^^^^^^^^^^^^^^^^^^^
TBD

Cables assembly
^^^^^^^^^^^^^^^
TBD
